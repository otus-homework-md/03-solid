﻿using solidApp.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solidApp
{
    internal class Validator : IValidator

    {
        
        int _answer =0;
        public int Answer { get => _answer; set => _answer=value; }

        public int CompareTo(IAnswer? other)
        {
            return other?.Answer-Answer??-1;
        }

        public ValidationResult Validate(IAnswer secret)
        {
            return this.CompareTo(secret)>0?ValidationResult.More:
                this.CompareTo(secret)<0?ValidationResult.Less:
                ValidationResult.Equal;
        }

    }
}
