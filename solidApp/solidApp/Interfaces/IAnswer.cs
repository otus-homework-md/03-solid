﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solidApp.Interfaces
{
    public interface IAnswer:IComparable<IAnswer>
    {
        /// <summary>
        /// Ответ
        /// </summary>
        public int Answer{get;set;}

    }
}
