﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solidApp.Interfaces
{
    internal interface IValidator:IAnswer
    {
        public ValidationResult Validate(IAnswer answer);
    }

    enum ValidationResult
    {
        Less=0,Equal,More
    }
}
