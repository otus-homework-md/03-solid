﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solidApp.Interfaces
{
    internal interface IGuessTimes
    {        
        /// <summary>
        /// Количество попыток угадать число
        /// </summary>
        public int TimesToGuess{get;}
    }
}
