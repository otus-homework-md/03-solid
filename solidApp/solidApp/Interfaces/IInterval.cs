﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solidApp.Interfaces
{
    internal interface IInterval
    {   
        /// <summary>
        /// Верхняя граница интервала
        /// </summary>
        public int Max {get;}

        /// <summary>
        /// Нижняя граница интервала
        /// </summary>
        public int Min {get;} 
    }
}
