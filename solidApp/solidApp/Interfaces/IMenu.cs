﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solidApp.Interfaces
{
    public interface IMenu
    {
        public bool IsEndOfProgram{get;}
        public void Menu();
        public State StateProg{ get; } 
    }

    public enum State
    {
        NextAttemp=0,
        End=1
    }
}
