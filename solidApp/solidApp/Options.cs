﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;
using solidApp.Interfaces;

namespace solidApp
{
    //принцип закрытости открытости - открыто для расширения закрыто для изменний
    //также можно демонстрировать принцип Лисков - рассматривая данный класс как базовый и вводя новые опции

    internal class Options : IOptions
    {
        public Options()
        {
            _ = int.TryParse(ConfigurationManager.AppSettings.Get("Attemps"), out _timesToGuess);
            _ = int.TryParse(ConfigurationManager.AppSettings.Get("MaxValue"), out _max);
            _ = int.TryParse(ConfigurationManager.AppSettings.Get("MinValue"), out _min);
        }

        int _timesToGuess = 1;
        public int TimesToGuess => _timesToGuess;

        int _max = 10;
        public int Max => _max;

        int _min = 0;
        public int Min => _min;
    }
}
