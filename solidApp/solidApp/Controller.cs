﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using solidApp.Interfaces;

namespace solidApp
{
    internal class Controller : IMenu
    {
        //Принцип инверсии зависимостей - зависимость от контрактов а не от их реализации

        private protected IOptions options;

        private protected IValidator validator;

        private protected int numberOfAttempts=0;

        private protected IAnswer secret;

        public Controller()
        {
            options=new Options();
            validator=new Validator();  
            secret=new Secret();
            Random rnd = new Random();
            secret.Answer=rnd.Next(options.Min,options.Max);

        }

        private protected bool _isEndOfProgram=false;
        public bool IsEndOfProgram 
          { 
            get =>_isEndOfProgram; 
          }
        

        private protected State _state=State.NextAttemp;
        public State StateProg 
          { 
            get => _state;            
          }

        public void Menu()
        {
            while (_state == State.NextAttemp)
            {
                Console.WriteLine($"У вас {options.TimesToGuess-numberOfAttempts} из {options.TimesToGuess} попыток , введите число из интервала от {options.Min} до {options.Max} :");
                numberOfAttempts++;

                _=int.TryParse(Console.ReadLine(), out int ins );
                validator.Answer=ins;
                
                var compareResult=validator.Validate(secret);

                switch (compareResult)
                {
                    case ValidationResult.Equal:
                        Console.WriteLine("угадали!");
                        _state=State.End;
                        break;
                    case ValidationResult.Less:
                        Console.WriteLine("Меньше!");
                        _state=State.NextAttemp;
                        break;
                    case ValidationResult.More:
                        Console.WriteLine("Больше!");
                        _state=State.NextAttemp;
                        break;
                }

                if (((options.TimesToGuess - numberOfAttempts) == 0)&&(compareResult!=ValidationResult.Equal))
                {
                    Console.WriteLine("Вы исчерпали все свои попытки!");
                    _state=State.End;
                }
                
            }
            

        }
    }
}
