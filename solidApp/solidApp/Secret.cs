﻿using solidApp.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solidApp
{
    public class Secret : IAnswer
    {
        
        int answer=0;
        public int Answer { 
                get =>  answer;
                set => answer=value; }

        public int CompareTo(IAnswer? other)
        {
            return other?.Answer-Answer??-1;
        }
    }
}
